/**
 * The MIT License
 * Copyright (c) 2015 Teal Cube Games
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package com.tealcube.minecraft.bukkit.hilt;

import org.bukkit.Material;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class HiltPotion extends HiltItemStack {

    public HiltPotion(PotionEffectType mainEffectType, Collection<PotionEffect> effects) {
        super(Material.POTION);
        setMainEffectType(mainEffectType);
        setEffects(effects);
    }

    public HiltPotion setMainEffectType(PotionEffectType type) {
        createItemMeta();
        if (getItemMeta() instanceof PotionMeta) {
            ((PotionMeta) getItemMeta()).setMainEffect(type);
        }
        return this;
    }

    public List<PotionEffect> getEffects() {
        createItemMeta();
        if (getItemMeta() instanceof PotionMeta && ((PotionMeta) getItemMeta()).hasCustomEffects()) {
            return new ArrayList<>(((PotionMeta) getItemMeta()).getCustomEffects());
        }
        return new ArrayList<>();
    }

    public HiltPotion setEffects(Collection<PotionEffect> effects) {
        createItemMeta();
        if (getItemMeta() instanceof PotionMeta) {
            if (((PotionMeta) getItemMeta()).hasCustomEffects()) {
                ((PotionMeta) getItemMeta()).clearCustomEffects();
            }
            for (PotionEffect potionEffect : effects) {
                ((PotionMeta) getItemMeta()).addCustomEffect(potionEffect, false);
            }
        }
        return this;
    }

}
